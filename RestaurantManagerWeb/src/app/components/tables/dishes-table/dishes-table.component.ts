import { element } from 'protractor';
import { RestaurantDTO } from './../../../services/restaurant/restaurantDTO';
import { RestaurantService } from './../../../services/restaurant/restaurant.service';
import { DishesService } from './../../../services/dishes/dishes.service';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { DishDTO } from './../../../services/dishes/dishDTO';
import { ITableComponent } from './../../../services/interfaces/ITableComponent';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-dishes-table',
  templateUrl: './dishes-table.component.html',
  styleUrls: ['./dishes-table.component.css']
})
export class DishesTableComponent implements OnInit, ITableComponent<DishDTO> {  
  displayDialog: boolean;
  data: DishDTO[];
  total: number;
  table: string;
  tableHeader: string;
  tableColumns: any[];
  selected: any;
  lastLazyEvent: LazyLoadEvent;

  restaurants: SelectItem[];
  selectedRestaurant: SelectItem;
  
  loadLazy(event: LazyLoadEvent) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
    
    this.service.getAll(event.first, event.rows)      
    .then( data =>
      {
        this.data = data.response;
        this.total = data.total;
      }
    );
  }

  constructor(private service: DishesService, private rService: RestaurantService) {
    this.restaurants = [];
    this.getAllRestaurants();
   }

  ngOnInit() {
    this.table = 'dishes';
    this.tableHeader = 'List of Dishes';
    this.tableColumns = [
      { name: 'name', value: 'Name' }, 
      { name: 'price', value: 'Price' }, 
      { name: 'restaurant.name', value: 'Restaurant' }
    ];
  }

  showDialogToAdd() {    
    this.selected = new DishDTO();
    this.displayDialog = true;
  }

  onRowSelect(event: any) {
    this.service.getByNameAndRestaurant(event.data.name, event.data.restaurant.name)      
    .then( data =>
      {
        if (data.response)
          this.selected = data.response;

        this.displayDialog = true;
      }
    );
  }

  save() {
    debugger;
    if (this.selected.id)
    {
      this.service.update(this.selected)      
      .then( data =>
        {
          if (data.response)
            this.selected = data.response;
  
          this.loadLazy(this.lastLazyEvent);
          this.displayDialog = false;
        }
      );
    } else {
      this.service.save(this.selected)      
      .then( data =>
        {
          if (data.response)
            this.selected = data.response;
  
          this.loadLazy(this.lastLazyEvent);
          this.displayDialog = false;
        }
      );
    }
  }

  delete() {
    if (this.selected.id)
    {
      this.service.delete(this.selected.id)      
      .then( data =>
        {  
          this.loadLazy(this.lastLazyEvent);
          this.displayDialog = false;
        }
      );
    }
  }

  getAllRestaurants(){
    this.rService.getAll(0, 0)      
    .then( data =>
      {
        debugger;
        data.response.forEach(element => {
          this.restaurants.push({ label: element.name, value: element.id });
        });
      }
    );
  }
}
