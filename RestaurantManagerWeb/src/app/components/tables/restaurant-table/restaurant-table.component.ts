import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { RestaurantService } from './../../../services/restaurant/restaurant.service';
import { RestaurantDTO } from './../../../services/restaurant/restaurantDTO';
import { ITableComponent } from './../../../services/interfaces/ITableComponent';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';

import {  } from 'primeng/components/common/lazyloadevent';


@Component({
  selector: 'app-restaurant-table',
  templateUrl: './restaurant-table.component.html',
  styleUrls: ['./restaurant-table.component.css']
})
export class RestaurantTableComponent implements OnInit, ITableComponent<RestaurantDTO> {
  
  data: RestaurantDTO[];
  total: number;
  table: string;
  tableHeader: string;
  tableColumns: string[];
  selected: any;
  displayDialog: boolean;
  lastLazyEvent: LazyLoadEvent;

  constructor(private service: RestaurantService) { }

  ngOnInit() {
    this.table = 'restaurants';
    this.tableHeader = 'List of Restaurants';
    this.tableColumns = ['Name'];
  }

  loadLazy(event: LazyLoadEvent) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
    this.lastLazyEvent = event;

    this.service.getAll(event.first, event.rows)      
    .then( data =>
      {
        this.data = data.response;
        this.total = data.total;
      }
    );
  }

  showDialogToAdd() {
    this.selected = new RestaurantDTO();
    this.displayDialog = true;
  }

  onRowSelect(event) {
    debugger;
    this.service.getByName(event.data['name'])      
    .then( data =>
      {
        if (data.response)
          this.selected = data.response;

        this.displayDialog = true;
      }
    );
  }

  save() {
    debugger;
    if (this.selected.id)
    {
      this.service.update(this.selected)      
      .then( data =>
        {
          if (data.response)
            this.selected = data.response;
  
          this.loadLazy(this.lastLazyEvent);
          this.displayDialog = false;
        }
      );
    } else {
      this.service.save(this.selected)      
      .then( data =>
        {
          if (data.response)
            this.selected = data.response;
  
          this.loadLazy(this.lastLazyEvent);
          this.displayDialog = false;
        }
      );
    }
  }

  delete() {
    if (this.selected.id)
    {
      this.service.delete(this.selected.id)      
      .then( data =>
        {  
          this.loadLazy(this.lastLazyEvent);
          this.displayDialog = false;
        }
      );
    }
  }
}