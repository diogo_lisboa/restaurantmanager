import { DishesService } from './services/dishes/dishes.service';
import { Http } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';
import { DataTableModule, SharedModule, CheckboxModule, DataListModule, 
  MenubarModule, DialogModule, InputTextModule, ButtonModule, DropdownModule } from 'primeng/primeng';

import { RestaurantService } from "./services/restaurant/restaurant.service";
import { RestaurantTableComponent } from './components/tables/restaurant-table/restaurant-table.component';
import { DishesTableComponent } from './components/tables/dishes-table/dishes-table.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RestaurantComponent,
    RestaurantTableComponent,
    DishesTableComponent
  ],
  imports: [
    BrowserModule,
    DataTableModule,
    SharedModule,
    HttpClientModule,
    DialogModule,
    InputTextModule,
    ButtonModule,
    CheckboxModule,
    DataListModule,
    MenubarModule,
    DropdownModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    RestaurantService,
    DishesService,
    HttpClient
  ],
  bootstrap: [
    AppComponent,
    HeaderComponent,
    RestaurantTableComponent,
    DishesTableComponent
  ]
})
export class AppModule { }
