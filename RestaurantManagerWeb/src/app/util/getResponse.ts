export interface GetResponse<T>{
    response: T[];
    total: number;
}