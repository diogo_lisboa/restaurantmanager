import { GetResponse } from './../../util/getResponse';
import { IService } from './../interfaces/IServices';
import { HeaderComponent } from './../../components/header/header.component';
import { RestaurantDTO } from './restaurantDTO';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from "@angular/common/http";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class RestaurantService implements IService {
  apiUrl: string;
  getAllUrl: string;
  getByNameUrl: string;
  saveUrl: string;
  updateUrl: string;
  deleteUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:83/';
    this.getAllUrl = 'restaurants/all?';
    this.getByNameUrl = 'restaurants/name?';
    this.saveUrl = 'restaurants/new';
    this.updateUrl = 'restaurants/update';
    this.deleteUrl = 'restaurants/delete';
   }

   getAll(skip: any, take: any) {
    let params = new HttpParams();
    params = params.append('s', skip);
    params = params.append('t', take);

    return this.http.get(this.apiUrl + this.getAllUrl, { params: params })
      .map(res => 
        res as GetResponse<RestaurantDTO>
      ).toPromise();
  }

  getByName(name: string) {
    let params = new HttpParams();
    params = params.append('r', name);

    return this.http.get(this.apiUrl + this.getByNameUrl, { params: params })
      .map(res => 
        res as GetResponse<RestaurantDTO>
      ).toPromise();
  }

  save(item: RestaurantDTO) {
    return this.http.post(this.apiUrl + this.saveUrl, item)
      .map(res => 
        res as GetResponse<RestaurantDTO>
      ).toPromise();
  }

  update(item: RestaurantDTO) {
    return this.http.post(this.apiUrl + this.updateUrl, item)
      .map(res => 
        res as GetResponse<RestaurantDTO>
      ).toPromise();
  }

  delete(itemId: any) {
    let params = new HttpParams();
    params = params.append('r', itemId);

    return this.http.get(this.apiUrl + this.deleteUrl, { params: params })
      .map(res => 
        res as GetResponse<RestaurantDTO>
      ).toPromise();
  }
}
