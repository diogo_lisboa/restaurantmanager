import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
export interface ITableComponent <T> {
    data: T[];
    total: number;
    table: string;
    tableHeader: string;
    tableColumns: any[];
    selected: any;
    displayDialog: boolean;

    loadLazy(event: LazyLoadEvent);
    showDialogToAdd();
    onRowSelect(event: any);    
    save();
    delete();
}