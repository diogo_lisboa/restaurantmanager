export interface IService{
    apiUrl: string;
    getAllUrl: string;
    getByNameUrl: string;
    saveUrl: string;
    updateUrl: string;
    deleteUrl: string;

    getAll(page, pageSize);
    getByName(name: string);    
    save(item: any);    
    update(item: any);    
    delete(itemId: any);
}