import { GetResponse } from './../../util/getResponse';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IService } from './../interfaces/IServices';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { DishDTO } from "./dishDTO";

@Injectable()
export class DishesService implements IService {    
  apiUrl: string;
  getAllUrl: string;
  getByNameUrl: string;
  saveUrl: string;
  updateUrl: string;
  deleteUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:83/dishes/';
    this.getAllUrl = 'all?';
    this.getByNameUrl = 'name';
    this.saveUrl = 'new';
    this.updateUrl = 'update';
    this.deleteUrl = 'delete';
  }

  getAll(page: any, pageSize: any) {
    //return ['R1','R2','R3']
    let params = new HttpParams();
    params = params.append('p', page);
    params = params.append('ps', pageSize);

    return this.http.get(this.apiUrl + this.getAllUrl, { params: params })
      .map(res => 
        res as GetResponse<DishDTO>
      ).toPromise();
  }

  getByName(name: string) {
    throw new Error("Method not implemented.");
  }

  getByNameAndRestaurant(name: string, restaurantName: string) {
    let params = new HttpParams();
    params = params.append('d', name);
    params = params.append('r', restaurantName);

    return this.http.get(this.apiUrl + this.getByNameUrl, { params: params })
      .map(res => 
        res as GetResponse<DishDTO>
      ).toPromise();
  }

  save(item: any) {    
    return this.http.post(this.apiUrl + this.saveUrl, item)
    .map(res => 
      res as GetResponse<DishDTO>
    ).toPromise();
  }

  update(item: any) {
    return this.http.post(this.apiUrl + this.updateUrl, item)
      .map(res => 
        res as GetResponse<DishDTO>
      ).toPromise();
  }
  
  delete(itemId: any) {
    let params = new HttpParams();
    params = params.append('r', itemId);

    return this.http.get(this.apiUrl + this.deleteUrl, { params: params })
      .map(res => 
        res as GetResponse<DishDTO>
      ).toPromise();
  }
}