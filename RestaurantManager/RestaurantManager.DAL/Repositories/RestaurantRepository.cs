﻿using RestaurantManager.DAL.Entities;
using RestaurantManager.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.DAL.Repositories
{
    /// <summary>
    /// Since EF already implements repository, this works as a way to centralize and contextualize all DB interactions.
    /// </summary>
    public class RestaurantRepository : GenericRepository<RestaurantEntity>
    {
        public override RestaurantEntity Save(RestaurantEntity entity)
        {
            using (var db = new ModelContext())
            {
                db.Restaurants.Add(entity);
                db.SaveChanges();
            };

            return entity;
        }

        public RestaurantEntity Update(RestaurantEntity entity)
        {
            using (var db = new ModelContext())
            {
                db.Restaurants.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            };

            return entity;
        }

        public override bool Delete(RestaurantEntity entity)
        {
            using (var db = new ModelContext())
            {
                db.Restaurants.Remove(entity);
                db.SaveChanges();
            };

            return true;
        }

        public bool Delete(int entityId)
        {
            using (var db = new ModelContext())
            {
                var entity = new RestaurantEntity() { Id = entityId };

                db.Restaurants.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            };

            return true;
        }

        public override RestaurantEntity GetById(long entityId)
        {
            using (var db = new ModelContext())
            {
                var query = from r in db.Restaurants
                            where r.Id == entityId
                            select r;

                return query.FirstOrDefault();
            };
        }

        public RestaurantEntity GetByName(string restaurant)
        {
            using (var db = new ModelContext())
            {
                var query = from r in db.Restaurants
                            where r.Name.ToLower().Equals(restaurant.ToLower())
                            select r;

                return query.FirstOrDefault();
            };
        }

        public override List<RestaurantEntity> GetAll(int skip, int take)
        {
            using (var db = new ModelContext())
            {
                var query = (from r in db.Restaurants
                            orderby r.Name
                            select r);

                if (skip == 0 && take == 0)
                    return query.ToList();
                else
                    return query.Skip(skip).Take(take).ToList();
            };
        }

        public long CountAll()
        {
            using (var db = new ModelContext())
            {
                return (from r in db.Restaurants
                             select r).Count();
            };
        }
    }
}
