﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.DAL.Repositories.Interfaces
{
    public abstract class GenericRepository<T>
    {
        public abstract List<T> GetAll(int page, int pageSize);
        public abstract T Save(T entity);
        public abstract bool Delete(T entity);
        public abstract T GetById(long entityId);
    }
}
