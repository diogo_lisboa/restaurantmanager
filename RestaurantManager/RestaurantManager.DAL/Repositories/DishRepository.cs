﻿using RestaurantManager.DAL.Entities;
using RestaurantManager.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.DAL.Repositories
{
    /// <summary>
    /// Since EF already implements repository, this works as a way to centralize and contextualize all DB interactions.
    /// </summary>
    public class DishRepository : GenericRepository<DishEntity>
    {
        public override DishEntity Save(DishEntity entity)
        {
            using (var db = new ModelContext())
            {
                db.Dishes.Add(entity);
                db.SaveChanges();
            };

            return entity;
        }

        public DishEntity Update(DishEntity entity)
        {
            using (var db = new ModelContext())
            {
                db.Dishes.Attach(entity);
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            };

            return entity;
        }

        public override bool Delete(DishEntity entity)
        {
            using (var db = new ModelContext())
            {
                db.Dishes.Remove(entity);
                db.SaveChanges();
            };

            return true;
        }

        public override DishEntity GetById(long entityId)
        {
            using (var db = new ModelContext())
            {
                var query = from d in db.Dishes
                            where d.Id == entityId
                            select d;

                return query.FirstOrDefault();
            };
        }
                
        public DishEntity GetByName(string dish, string restaurant)
        {
            using (var db = new ModelContext())
            {
                var query = from d in db.Dishes.Include("Restaurant")
                            where d.Name.ToLower().Equals(dish.ToLower()) &&
                                d.Restaurant != null &&
                                d.Restaurant.Name.ToLower().Equals(restaurant.ToLower())
                            select d;

                return query.FirstOrDefault();
            };
        }

        public List<DishEntity> GetAllByRestaurant(long restaurantId, int page, int pageSize)
        {
            using (var db = new ModelContext())
            {
                var query = (from d in db.Dishes
                            where d.RestaurantId == restaurantId
                            orderby d.Name
                            select d).Skip(page * pageSize).Take(pageSize);

                return query.ToList();
            };
        }

        public override List<DishEntity> GetAll(int page, int pageSize)
        {
            using (var db = new ModelContext())
            {
                var query = (from d in db.Dishes.Include("Restaurant")
                             orderby d.Name
                             select d).Skip(page * pageSize).Take(pageSize);

                return query.ToList();
            };
        }
        public long CountAll()
        {
            using (var db = new ModelContext())
            {
                return (from r in db.Dishes
                        select r).Count();
            };
        }
    }
}
