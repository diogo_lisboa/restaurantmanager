﻿using RestaurantManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create and save a new Blog 
            Console.Write("Enter a name for a new Restaurant: ");
            var name = Console.ReadLine();

            var restaurant = new RestaurantEntity { Name = name };

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
