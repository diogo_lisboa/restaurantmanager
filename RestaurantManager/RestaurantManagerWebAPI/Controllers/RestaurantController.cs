﻿using RestaurantManager.Biz.DTO;
using RestaurantManager.Biz.Helper;
using RestaurantManager.Biz.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RestaurantManagerWebAPI.Controllers
{
    [RoutePrefix("restaurants")]
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class RestaurantController : ApiController
    {
        /// <summary>
        /// Returns all the restaurants
        /// </summary>
        /// <returns></returns>
        [Route("all")]
        [HttpGet]
        public GenericResponse<List<RestaurantDTO>> GetAllRestaurants(int s, int t, bool d = false)
        {
            try
            {
                return BizManager.Instance.RestaurantBiz.GetAllRestaurants(s, t, d);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the restaurant by its id.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        [Route("{r:long}")]
        [HttpGet]
        public GenericResponse<RestaurantDTO> GetRestaurantById(long r)
        {
            try
            {
                return BizManager.Instance.RestaurantBiz.GetRestaurantById(r);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the restaurant by its id.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        [Route("name")]
        [HttpGet]
        public GenericResponse<RestaurantDTO> GetRestaurantByName(string r)
        {
            try
            {
                return BizManager.Instance.RestaurantBiz.GetRestaurantByName(r);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validates and saves the restaurant.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Route("new")]
        [HttpPost]
        public GenericResponse<RestaurantDTO> CreateRestaurant(RestaurantDTO dto)
        {
            try
            {
                return BizManager.Instance.RestaurantBiz.SaveRestaurant(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validates and updates the restaurant.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Route("update")]
        [HttpPost]
        public GenericResponse<RestaurantDTO> UpdateRestaurant(RestaurantDTO dto)
        {
            try
            {
                return BizManager.Instance.RestaurantBiz.UpdateRestaurant(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the restaurant by its id.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        [Route("delete")]
        [HttpGet]
        public GenericResponse<bool> DeleteRestaurantById(int r)
        {
            try
            {
                return BizManager.Instance.RestaurantBiz.DeleteRestaurant(r);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
