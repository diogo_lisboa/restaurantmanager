﻿using RestaurantManager.Biz.DTO;
using RestaurantManager.Biz.Helper;
using RestaurantManager.Biz.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RestaurantManagerWebAPI.Controllers
{
    [RoutePrefix("dishes")]
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class DishController : ApiController
    {
        /// <summary>
        /// Returns all the dishes.
        /// </summary>
        /// <returns></returns>
        [Route("all")]
        [HttpGet]
        public GenericResponse<List<DishDTO>> GetAllRestaurants(int p, int ps)
        {
            try
            {
                return BizManager.Instance.DishBiz.GetAll(p, ps);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the dish by its id.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        [Route("{d:long}")]
        [HttpGet]
        public GenericResponse<DishDTO> GetDishById(long d)
        {
            try
            {
                return BizManager.Instance.DishBiz.GetDishById(d);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the dish by its name and restaurant.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        [Route("name")]
        [HttpGet]
        public GenericResponse<DishDTO> GetDishByName(string d, string r)
        {
            try
            {
                return BizManager.Instance.DishBiz.GetDishByName(d, r);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validates and saves the dish.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Route("new")]
        [HttpPost]
        public GenericResponse<DishDTO> CreateDish(DishDTO dto)
        {
            try
            {
                return BizManager.Instance.DishBiz.SaveDish(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Validates and updates the dish.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [Route("update")]
        [HttpPost]
        public GenericResponse<DishDTO> UpdateDish(DishDTO dto)
        {
            try
            {
                return BizManager.Instance.DishBiz.UpdateDish(dto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the dish by its id.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        [Route("delete")]
        [HttpGet]
        public GenericResponse<bool> DeleteDishById(int d)
        {
            try
            {
                return BizManager.Instance.DishBiz.DeleteDish(d);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns all the dishes under the informed restaurantId.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        [Route("restaurant/{r:long}")]
        [HttpGet]
        public GenericResponse<List<DishDTO>> GetRestaurantDishes(long r, int p, int ps)
        {
            try
            {
                return BizManager.Instance.DishBiz.GetRestaurantDishes(r, p, ps);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
