﻿using RestaurantManager.Biz.DTO;
using RestaurantManager.Biz.Helper;
using RestaurantManager.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.Biz.Biz
{
    public class DishBiz
    {
        #region Constructor

        private DishRepository _dishRepo;

        public DishBiz()
        {
            if (_dishRepo == null)
                _dishRepo = new DishRepository();
        }

        #endregion

        public GenericResponse<List<DishDTO>> GetAll(int page, int pageSize)
        {
            var result = new GenericResponse<List<DishDTO>>();

            result.Response = DishHelper.ConvertToDTO(
                _dishRepo.GetAll(page, pageSize));

            result.Total = _dishRepo.CountAll();

            return result;
        }

        /// <summary>
        /// Finds the dish by its id.
        /// </summary>
        /// <param name="dishId"></param>
        /// <returns></returns>
        public GenericResponse<DishDTO> GetDishById(long dishId)
        {
            var result = new GenericResponse<DishDTO>();

            if (dishId == 0)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_ID,
                    StatusMessage = "Id is invalid."
                });
            }

            result.Response = DishHelper.ConvertToDTO(
                _dishRepo.GetById(dishId));

            return result;
        }


        public GenericResponse<DishDTO> GetDishByName(string dish, string restaurant)
        {
            var result = new GenericResponse<DishDTO>();

            if (string.IsNullOrEmpty(dish))
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_NAME,
                    StatusMessage = "Name is invalid."
                });
            }

            if (string.IsNullOrEmpty(restaurant))
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_RESTAURANT,
                    StatusMessage = "Restaurant is invalid."
                });
            }

            result.Response = DishHelper.ConvertToDTO(
                _dishRepo.GetByName(dish, restaurant));

            return result;
        }

        /// <summary>
        /// Finds all the dishes under the informed restaurantId
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public GenericResponse<List<DishDTO>> GetRestaurantDishes(long restaurantId, int page, int pageSize)
        {
            var result = new GenericResponse<List<DishDTO>>();

            if (restaurantId == 0)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_RESTAURANT,
                    StatusMessage = "RestaurantId is invalid."
                });
            }

            result.Response = DishHelper.ConvertToDTO(
                _dishRepo.GetAllByRestaurant(restaurantId, page, pageSize));

            return result;
        }

        /// <summary>
        /// Validates and saves the dish.
        /// </summary>
        /// <param name="dishDTO"></param>
        /// <returns></returns>
        public GenericResponse<DishDTO> SaveDish(DishDTO dishDTO)
        {
            var result = new GenericResponse<DishDTO>();

            if (!dishDTO.IsValidName)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_NAME,
                    StatusMessage = "Name is null or empty."
                });
            }

            if (!dishDTO.IsValidRestaurant)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_RESTAURANT,
                    StatusMessage = "RestaurantId is invalid."
                });
            }

            if (!dishDTO.IsValidPrice)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_PRICE,
                    StatusMessage = "Price is invalid."
                });
            }

            if (result.Status == GenericStatus.OK)
                result.Response = DishHelper.ConvertToDTO(
                    _dishRepo.Save(
                        DishHelper.ConvertToEntity(dishDTO)));

            return result;
        }
        
        public GenericResponse<DishDTO> UpdateDish(DishDTO dishDTO)
        {
            var result = new GenericResponse<DishDTO>();

            if (!dishDTO.IsValidName)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_NAME,
                    StatusMessage = "Name is null or empty."
                });
            }

            if (!dishDTO.IsValidRestaurant)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_RESTAURANT,
                    StatusMessage = "RestaurantId is invalid."
                });
            }

            if (!dishDTO.IsValidPrice)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_PRICE,
                    StatusMessage = "Price is invalid."
                });
            }

            if (result.Status == GenericStatus.OK)
                result.Response = DishHelper.ConvertToDTO(
                    _dishRepo.Update(
                        DishHelper.ConvertToEntity(dishDTO)));

            return result;
        }

        /// <summary>
        /// Deletes the dish by its id.
        /// </summary>
        /// <param name="dishId"></param>
        /// <returns></returns>
        public GenericResponse<bool> DeleteDish(long dishId)
        {
            var result = new GenericResponse<bool>();

            if (dishId == 0)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_ID,
                    StatusMessage = "Id is invalid."
                });
            }

            if (result.Status != GenericStatus.OK)
                return result;

            var dishEntity = _dishRepo.GetById(dishId);

            if (dishEntity == null)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_ID,
                    StatusMessage = "Dish not found by the informed id."
                });
            }
            else
                result.Response = _dishRepo.Delete(dishEntity);

            return result;
        }
    }
}
