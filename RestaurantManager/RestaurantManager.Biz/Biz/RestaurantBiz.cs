﻿using RestaurantManager.Biz.DTO;
using RestaurantManager.Biz.Helper;
using RestaurantManager.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.Biz.Biz
{
    public class RestaurantBiz
    {
        #region Constructor

        private RestaurantRepository _restaurantRepo;

        public RestaurantBiz()
        {
            if (_restaurantRepo == null)
                _restaurantRepo = new RestaurantRepository();
        }

        #endregion

        /// <summary>
        /// Finds the restaurant by its id.
        /// </summary>
        /// <param name="restaurantID"></param>
        /// <returns></returns>
        public GenericResponse<RestaurantDTO> GetRestaurantById(long restaurantId)
        {
            var result = new GenericResponse<RestaurantDTO>();

            if (restaurantId == 0)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_ID,
                    StatusMessage = "Id is invalid."
                });
            }

            result.Response = RestaurantHelper.ConvertToDTO(
                _restaurantRepo.GetById(restaurantId));

            return result;
        }

        /// <summary>
        /// Finds all the restaurants.
        /// </summary>
        /// <returns></returns>
        public GenericResponse<List<RestaurantDTO>> GetAllRestaurants(int skip, int take, bool d)
        {
            var result = new GenericResponse<List<RestaurantDTO>>();

            result.Response = RestaurantHelper.ConvertToDTO(
                _restaurantRepo.GetAll(skip, take));

            result.Total = _restaurantRepo.CountAll();

            return result;
        }

        public GenericResponse<RestaurantDTO> GetRestaurantByName(string restaurant)
        {
            var result = new GenericResponse<RestaurantDTO>();

            if (string.IsNullOrEmpty(restaurant))
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_NAME,
                    StatusMessage = "Name is invalid."
                });
            }

            result.Response = RestaurantHelper.ConvertToDTO(
                _restaurantRepo.GetByName(restaurant));

            return result;
        }

        /// <summary>
        /// Validates and saves the restaurant.
        /// </summary>
        /// <param name="restaurantDTO"></param>
        /// <returns></returns>
        public GenericResponse<RestaurantDTO> SaveRestaurant(RestaurantDTO restaurantDTO)
        {
            var result = new GenericResponse<RestaurantDTO>();

            if (!restaurantDTO.IsValidName)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_NAME,
                    StatusMessage = "Name is null or empty."
                });
            }

            if (result.Status == GenericStatus.OK)
                result.Response = RestaurantHelper.ConvertToDTO(
                    _restaurantRepo.Save(
                        RestaurantHelper.ConvertToEntity(restaurantDTO)));

            return result;
        }

        /// <summary>
        /// Validates and updates the restaurant.
        /// </summary>
        /// <param name="restaurantDTO"></param>
        /// <returns></returns>
        public GenericResponse<RestaurantDTO> UpdateRestaurant(RestaurantDTO restaurantDTO)
        {
            var result = new GenericResponse<RestaurantDTO>();

            if (restaurantDTO.Id == 0)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_ID,
                    StatusMessage = "Id is invalid."
                });
            }

            if (!restaurantDTO.IsValidName)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_NAME,
                    StatusMessage = "Name is null or empty."
                });
            }

            if (result.Status == GenericStatus.OK)
                result.Response = RestaurantHelper.ConvertToDTO(
                    _restaurantRepo.Update(
                        RestaurantHelper.ConvertToEntity(restaurantDTO)));

            return result;
        }

        /// <summary>
        /// Deletes the restaurant by its id.
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public GenericResponse<bool> DeleteRestaurant(int restaurantId)
        {
            var result = new GenericResponse<bool>();

            if (restaurantId == 0)
            {
                result.Status = GenericStatus.ERROR;
                result.Errors.Add(new RequestError()
                {
                    Status = ErrorStatus.INVALID_ID,
                    StatusMessage = "Id is invalid."
                });
            }

            if (result.Status != GenericStatus.OK)
                return result;
            
            result.Response = _restaurantRepo.Delete(restaurantId);

            return result;
        }
    }
}
