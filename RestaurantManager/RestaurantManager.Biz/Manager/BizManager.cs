﻿using RestaurantManager.Biz.Biz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.Biz.Manager
{
    public class BizManager
    {
        #region Singleton instance

        protected static object _syncRoot = new Object();
        private static volatile BizManager instance;

        private RestaurantBiz _restBiz;
        private DishBiz _dishBiz;

        private BizManager()
        {
            if (_restBiz == null)
                _restBiz = new RestaurantBiz();

            if (_dishBiz == null)
                _dishBiz = new DishBiz();
        }

        public static BizManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (instance == null)
                            instance = new BizManager();
                    }
                }

                return instance;
            }
        }

        #endregion

        public RestaurantBiz RestaurantBiz
        {
            get
            {
                return _restBiz;
            }
        }

        public DishBiz DishBiz
        {
            get
            {
                return _dishBiz;
            }
        }
    }
}
