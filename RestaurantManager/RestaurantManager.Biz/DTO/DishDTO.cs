﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.Biz.DTO
{
    [DataContract]
    public class DishDTO
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "restaurantId")]
        public int RestaurantId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "price")]
        public string Price { get; set; }

        [DataMember(Name = "restaurant")]
        public RestaurantDTO Restaurant { get; set; }


        public bool IsValidId
        {
            get
            {
                return Id > 0;
            }
        }

        public bool IsValidName
        {
            get
            {
                return !string.IsNullOrEmpty(Name);
            }
        }

        public bool IsValidRestaurant
        {
            get
            {
                return RestaurantId > 0;
            }
        }

        public bool IsValidPrice
        {
            get
            {
                decimal temp;
                return !string.IsNullOrEmpty(Price) && decimal.TryParse(Price, out temp);
            }
        }
    }
}
