﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.Biz.DTO
{
    [DataContract]
    public class RestaurantDTO
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "dishes")]
        public List<DishDTO> Dishes { get; set; }


        public RestaurantDTO()
        {
            Dishes = new List<DishDTO>();
        }

        public bool IsValidName
        {
            get
            {
                return !string.IsNullOrEmpty(Name);
            }
        }
    }
}
