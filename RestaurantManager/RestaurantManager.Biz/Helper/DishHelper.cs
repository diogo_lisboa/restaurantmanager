﻿using RestaurantManager.Biz.DTO;
using RestaurantManager.DAL.Entities;
using System.Collections.Generic;

namespace RestaurantManager.Biz.Helper
{
    public static class DishHelper
    {
        public static DishDTO ConvertToDTO(DishEntity entity)
        {
            if (entity == null)
                return null;

            return new DishDTO()
            {
                Id = entity.Id,
                RestaurantId = entity.RestaurantId,
                Name = entity.Name,
                Price = entity.Price
            };
        }

        public static List<DishDTO> ConvertToDTO(List<DishEntity> entities)
        {
            if (entities == null)
                return null;

            var result = new List<DishDTO>();
            foreach (var entity in entities)
            {
                result.Add(new DishDTO()
                {
                    Id = entity.Id,
                    RestaurantId = entity.RestaurantId,
                    Name = entity.Name,
                    Price = entity.Price,
                    Restaurant = RestaurantHelper.ConvertToDTO(entity.Restaurant)
                });
            }

            return result;
        }

        public static DishEntity ConvertToEntity(DishDTO dto)
        {
            if (dto == null)
                return null;

            return new DishEntity()
            {
                Id = dto.Id,
                RestaurantId = dto.RestaurantId,
                Name = dto.Name,
                Price = dto.Price
            };
        }

        public static List<DishEntity> ConvertToDTO(List<DishDTO> dtos)
        {
            if (dtos == null)
                return null;

            var result = new List<DishEntity>();
            foreach (var dto in dtos)
            {
                result.Add(new DishEntity()
                {
                    Id = dto.Id,
                    RestaurantId = dto.RestaurantId,
                    Name = dto.Name,
                    Price = dto.Price
                });
            }

            return result;
        }
    }
}
