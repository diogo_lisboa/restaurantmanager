﻿using RestaurantManager.Biz.DTO;
using RestaurantManager.DAL.Entities;
using System.Collections.Generic;

namespace RestaurantManager.Biz.Helper
{
    public static class RestaurantHelper
    {
        public static RestaurantDTO ConvertToDTO(RestaurantEntity entity)
        {
            if (entity == null)
                return null;

            return new RestaurantDTO()
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public static List<RestaurantDTO> ConvertToDTO(List<RestaurantEntity> entities)
        {
            if (entities == null)
                return null;

            var result = new List<RestaurantDTO>();
            foreach (var entity in entities)
            {
                result.Add(new RestaurantDTO()
                {
                    Id = entity.Id,
                    Name = entity.Name
                });
            }

            return result;
        }

        public static RestaurantEntity ConvertToEntity(RestaurantDTO dto)
        {
            if (dto == null)
                return null;

            return new RestaurantEntity()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public static List<RestaurantEntity> ConvertToDTO(List<RestaurantDTO> dtos)
        {
            if (dtos == null)
                return null;

            var result = new List<RestaurantEntity>();
            foreach (var dto in dtos)
            {
                result.Add(new RestaurantEntity()
                {
                    Id = dto.Id,
                    Name = dto.Name
                });
            }

            return result;
        }
    }
}
