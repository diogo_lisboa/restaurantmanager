﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManager.Biz.Helper
{
    [DataContract]
    public class GenericResponse<T>
    {
        [DataMember(Name = "response")]
        public T Response { get; set; }

        [DataMember(Name = "total")]
        public long Total { get; set; }

        [DataMember(Name = "status")]
        public GenericStatus Status { get; set; }

        [DataMember(Name = "errors")]
        public List<RequestError> Errors { get; set; }

        public GenericResponse()
        {
            Errors = new List<RequestError>();
        }
    }

    [DataContract]
    public class RequestError
    {
        [DataMember(Name = "status")]
        public ErrorStatus Status { get; set; }

        [DataMember(Name = "statusMessage")]
        public string StatusMessage { get; set; }
    }

    public enum ErrorStatus
    {
        INTERNAL_ERROR, INVALID_ID, INVALID_NAME, INVALID_PRICE, INVALID_RESTAURANT
    }

    public enum GenericStatus
    {
        OK, ERROR
    }
}
